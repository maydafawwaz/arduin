#define kaki1maju 6
#define kaki1mundur 13  
#define kaki2maju 8
#define kaki2mundur 11
#define kaki3maju 10
#define kaki3mundur 9
#define kaki4maju 7
#define kaki4mundur 12
int pwm,pwm1,pwm2,pwm3,pwm4;
char c;
void kaki1(int k){
  analogWrite(kaki1maju,k);
  analogWrite(kaki1mundur,0);  
}

void kaki2(int k){
  analogWrite(kaki2maju,k);
  analogWrite(kaki2mundur,0);  
}

void kaki3(int k){
  analogWrite(kaki3maju,k);
  analogWrite(kaki3mundur,0);  
}

void kaki4(int k){
  analogWrite(kaki4maju,k);
  analogWrite(kaki4mundur,0);  
}
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  c='a';
  setpwm(40);
}
void info(){
  Serial.print(c);
  Serial.print(" ");
  Serial.println(pwm);  
}
void setpwm(int k){
    pwm=k;
    pwm1=pwm;
    pwm2=pwm;
    pwm3=pwm;
    pwm4=pwm;
}

void loop() {
  // put your main code here, to run repeatedly:
  if(Serial.available()){
    c=Serial.read();
    }

    if(c=='s'){
        Serial.println("Stopped");
        kaki1(0);
        kaki2(0);
        kaki3(0);
        kaki4(0);
      } else if (c=='a'){
        info();
        kaki1(pwm1);
        kaki2(0);
        kaki3(pwm3);
        kaki4(0);
      } else if(c=='b'){
        info();
        kaki1(0);
        kaki2(pwm2);
        kaki3(0);
        kaki4(pwm4);
      } else if(c=='f'){
        Serial.print("Set pwm: ");
        pwm=Serial.parseInt();
        Serial.print("PWM setted ");
        Serial.println(pwm);
        setpwm(pwm);
        c='s';
        }
}
